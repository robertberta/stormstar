package com.daydev.stormstar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by robert on 22.06.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentConsumerTest {
    //    @MockBean
    //    private S3Helper s3Helper;
    @Autowired
    DocumentConsumer documentConsumer;
    @MockBean
    S3Helper s3Helper;

    @Test
    public void testConsumer() throws InterruptedException, IOException {
        documentConsumer.start();
        String filePath = DocumentConsumer.BASE_PATH + "/visma/invoice/2017-12-30/part-0000";

        createTestFile(filePath);

        when(s3Helper.createFile(any(String.class),any(InputStream.class),any(Long.class))).thenReturn(true);
        Thread.sleep(1000);
        documentConsumer.shutdown();

        Assert.assertFalse(Files.exists(Paths.get(filePath)));
    }


    private void createTestFile(String filePath) throws IOException {

        Files.createDirectories(Paths.get(filePath).getParent());

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("valid/part-0000");

        Files.copy(inputStream, Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
    }


}
