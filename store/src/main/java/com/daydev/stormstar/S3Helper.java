package com.daydev.stormstar;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.couchbase.CouchbaseProperties;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by robert on 11.06.2017.
 */
@Component
public class S3Helper {
    private AmazonS3 s3client;
    private final String SUFFIX = "/";
    private final String BUCKETNAME = "sample";
    Logger log = Logger.getLogger(this.getClass());

    public S3Helper() {
        // credentials object identifying user for authentication
        // user must have AWSConnector and AmazonS3FullAccess for
        // this example to work
        AWSCredentials credentials = new BasicAWSCredentials(
                "YourAccessKeyID",
                "YourSecretAccessKey");

        // create a client connection based on credentials
        s3client = new AmazonS3Client(credentials);
    }

    public List<String> listBucket() {
        return s3client.listBuckets().stream()
                .map(bucket -> bucket.getName())
                .collect(Collectors.toList());
    }



    public boolean createFolder(String folderName) {
        Boolean success = true;
        // create meta-data for your folder and set content-length to 0
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        // create empty content
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        // create a PutObjectRequest passing the folder name suffixed by /
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKETNAME,
                    folderName + "/", emptyContent, metadata);
            // send request to S3 to create folder
            s3client.putObject(putObjectRequest);
        } catch (AmazonClientException ex) {
            success = false;
            log.error(ex.getMessage());
        }
        return success;
    }

    public boolean createFile(String fullName, InputStream inputStream, Long length) {
        Boolean success = true;

        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(length);
            s3client.putObject(new PutObjectRequest(BUCKETNAME, fullName, inputStream, metadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead));

        } catch (AmazonClientException ex) {
            success = false;
            log.error(ex.getMessage());
        }
        return success;

    }

    // upload file to folder and set it to public


}
