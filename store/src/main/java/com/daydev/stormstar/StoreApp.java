package com.daydev.stormstar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
//@ComponentScan({"com.daydev.stormstar.ruleengine","com.daydev.stormstar.facade"})
@EnableDiscoveryClient
public class StoreApp {
    @Autowired
    private static DocumentConsumer documentConsumer;

    public static void main(String[] args) {

//        SpringApplication.run(StoreApp.class, args);

        System.out.println("Press CTRL+C to exit");
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                System.out.println("Shutdown requested");
                documentConsumer.shutdown();
                System.out.println("Shutdown complete");
            }
        });



    }
}
