package com.daydev.stormstar;

import com.daydev.stormstar.S3Helper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by robert on 11.06.2017.
 */
@Component
public class DocumentConsumer {
    private Logger log = Logger.getLogger(this.getClass());
    private final static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final static int POOL_MINS = 1;
    public final static String BASE_PATH = "ssdata";
    private final static String BASE_NAME = "part";
    private final static String CURRENT_FILE_NAME = "current";
    private volatile boolean shutdown = false;
    @Autowired
    private volatile S3Helper s3Helper;
    @Autowired
    private volatile DocumentRepository documentRepository;

    public DocumentConsumer() {
    }

    public void start(){
        Runnable myRunnable = new Runnable() {

            public void run() {
                int cnt = 0;
                while (shutdown == false) {
                    if (cnt++ % 1000 == 0) {
                        try {
                            saveAll();
                        } catch (Exception ex) {
                            log.error(ex);

                        }
                    }
                    try {
                        Thread.sleep(POOL_MINS * 60);
                    } catch (InterruptedException ex) {
                        log.error(ex,ex);
                    }
                }
                shutdown = false;
            }
        };

        Thread thread = new Thread(myRunnable);
        thread.start();
    }

    public void saveAll() throws IOException {
        List<Path> companies = Files.list(Paths.get(BASE_PATH))
                .filter(file -> Files.isDirectory(file))
                .collect(Collectors.toList());
        for (Path companie : companies) {
            List<Path> documents = Files.list(companie)
                    .filter(file -> Files.isDirectory(file))
                    .collect(Collectors.toList());
            for (Path document : documents) {
                List<Path> dates = Files.list(document)
                        .filter(file -> Files.isDirectory(file))
                        .collect(Collectors.toList());
                for (Path date : dates) {
                    List<Path> files = Files.list(date)
                            .filter(file -> Files.isRegularFile(file))
                            .filter(file -> file.getFileName().toString().contains(BASE_NAME))
                            .collect(Collectors.toList());
                    for (Path file : files) {
                        long numTries = 0;
                        boolean uploaded = false;
                        do {
                            String suffix = "";
                            if (numTries > 0) {
                                suffix = "-" + numTries;
                            }
                            uploaded = s3Helper.createFile(file.toFile().getAbsolutePath() + suffix, Files.newInputStream(file), Files.size(file));
                        }
                        while (uploaded == false && ++numTries < 3);
                        if (uploaded) {
                            Files.delete(file);
                        }
                    }
                }
            }

        }
    }

    public void shutdown() {
        shutdown = true;
        while (shutdown) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                log.error(ex.getMessage());
            }

        }
    }
}