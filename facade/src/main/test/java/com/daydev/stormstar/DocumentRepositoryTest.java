package com.daydev.stormstar;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DocumentRepositoryTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DocumentRepository documentRepository;

    private static Gson gson = new Gson();

    @Before
    public void deleteDocuments() {
        documentRepository.deleteAll();
    }

    private ObjectMapper objectMapper;

    @Test
    public void documentIsCreated() throws Exception {

        //given
        String jsonDocument = TestUtils.resourceFileContent("valid/document.json");
        List<String> jsonFields = TestUtils.resourceFolderFileContents("valid/fields");

        //when
        this.mockMvc.perform(post("/document").content(jsonDocument)).andDo(print()).andExpect(status().isCreated());

        for (String jsonField : jsonFields) {
            this.mockMvc.perform(post("/field").content(jsonField)).andDo(print()).andExpect(status().isCreated());
        }

        //than
        Document document = documentRepository.findByCompanyAndName("Visma", "invoice");
        assertEquals(document.getFields().size(), 2);
    }

    @Ignore
    @Test
    public void paramGreetingShouldReturnTailoredMessage() throws Exception {

        this.mockMvc.perform(get("/greeting").param("name", "Spring Community"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.content").value("Hello, Spring Community!"));
    }
}
