package com.daydev.stormstar;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentObjRepository extends CrudRepository<DocumentObj, Long> {
    DocumentObj findOne(Long id);

    DocumentObj findByCompanyAndName(String company, String name);
}