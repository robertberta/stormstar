package com.daydev.stormstar;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import javax.persistence.*;

/**
 * Created by robert on 27.06.2017.
 */
@Entity
@Getter
@Setter
public class DocumentObj {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "nameklsx")
    String name;
    @Column(name = "dateklsx")
    Long date;
    String field1;
    String field2;
    String field3;
    String field4;
    String field5;
    String content;
}
