package com.daydev.stormstar;

/**
 * Created by robert on 27.05.2017.
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "field")
public interface FieldRepository extends CrudRepository<Field, Integer> {
}