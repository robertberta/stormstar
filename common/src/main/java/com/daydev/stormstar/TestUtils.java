package com.daydev.stormstar;

/**
 * Created by robert on 28.05.2017.
 */

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    private static ClassLoader classLoader = TestUtils.class.getClassLoader();

    public static String resourceFileContent(String file) throws IOException {
        InputStream fileContent = classLoader.getResourceAsStream(file);
        return IOUtils.toString(fileContent);
    }

    public static List<String> resourceFolderFileContents(String folder) {
        List<String> result = new ArrayList<>();

        if (!folder.endsWith("/")) {
            folder = folder + "/";
        }
        InputStream streamFileNames = classLoader.getResourceAsStream(folder + ".");

        try {
            String strFileNames = IOUtils.toString(streamFileNames);
            String[] fileNames = strFileNames.split("\n");

            for (int i = 0; i < fileNames.length; i++) {
                String fileContent = resourceFileContent(folder + fileNames[i]);
                result.add(fileContent);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
