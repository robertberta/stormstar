package com.daydev.stormstar;

import lombok.*;

import javax.persistence.*;

/**
 * Created by robert on 27.05.2017.
 */
@Entity
@Value
@NoArgsConstructor(force = true)
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "idDocument"})})
public class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idField;

    private String name;
    private String dataType;
    private String constraints;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "idDocument")
    private Document document;
}
