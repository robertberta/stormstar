package com.daydev.stormstar;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"company", "name"})})
public class Document implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String company;
    private String name;

    @Setter
    private Integer partNo = 0;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "document")
    private List<Field> fields;

    private String field1, field2, field3, field4, field5;
}
