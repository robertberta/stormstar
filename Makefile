all : start
build: stopImage
	docker rm stormstar.mysql || true
	docker build -f deploy/mysql/Dockerfile -t stormstar.mysql .
	docker run -d -p 3306:3306 --name "stormstar.mysql" stormstar.mysql
start:
	docker stop "stormstar.mysql"
	docker start stormstar.mysql
stop : stopImage
stopImage :
	docker stop "stormstar.mysql" || true
	#docker rm "stormstar.mysql"
