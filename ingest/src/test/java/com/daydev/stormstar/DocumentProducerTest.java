package com.daydev.stormstar;

import com.daydev.stormstar.Document;
import com.daydev.stormstar.DocumentRepository;
import com.daydev.stormstar.TestUtils;
import com.daydev.stormstar.ingest.DocumentProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

/**
 * Created by robert on 08.06.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentProducerTest {

    @Autowired
    DocumentProducer documentProducer;
    @Autowired
    DocumentRepository documentRepository;

    @Test
    public void send() throws IOException {
        Document document = documentRepository.findByCompanyAndName("Visma", "invoice");

        List<String> jsons = TestUtils.resourceFolderFileContents("valid");
        for (int i=0;i<1000;i++)
        for (String json : jsons) {
            documentProducer.send(document, json);
        }

    }
}
