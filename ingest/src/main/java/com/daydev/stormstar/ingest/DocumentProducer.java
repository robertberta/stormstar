package com.daydev.stormstar.ingest;

/**
 * Created by robert on 28.05.2017.
 */

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.daydev.stormstar.Document;
import com.daydev.stormstar.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class DocumentProducer {

    @Autowired
    DocumentRepository documentRepository;

    private final static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final static String PATH_DELIMITER="/";
    private final static String BASE_PATH ="ssdata";
    private final static String BASE_NAME = "part";
    private final static String CURRENT_FILE_NAME = "current";
    private final static Long FILE_SIZE_LIMIT = 10000L;

    public DocumentProducer() {
    }


    public void send(Document document, String json) throws IOException{
        String folder = filePath(document);
        Files.createDirectories(Paths.get(folder));
        File file = new File(folder + CURRENT_FILE_NAME);

        file.createNewFile();

        Files.write(file.toPath(),Arrays.asList(json), Charset.forName("UTF-8"), StandardOpenOption.APPEND);

        if (file.length() > FILE_SIZE_LIMIT){
            File newFile = new File(filePath(document) + fileName(document));
            file.renameTo(newFile);
        }


    }

    public void close() {
    }

    private String fileName(Document document){
        Document latest = documentRepository.findByCompanyAndName(document.getCompany(),document.getName());
        Integer partNo = latest.getPartNo();
        latest.setPartNo(partNo + 1);
        documentRepository.save(latest);
        return BASE_NAME + String.format("%05d", partNo);
    }

    private String filePath(Document document) {
        Date date = new Date();
        String strDate = dateFormat.format(date);
        List<String> folders = Arrays.asList(BASE_PATH,document.getCompany(),document.getName(),strDate);
        StringBuilder path = new StringBuilder();
        for (String folder:folders){
            path = path.append(folder)
                    .append(PATH_DELIMITER);
        }
        return path.toString();
    }
}
