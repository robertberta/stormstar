package com.daydev.stormstar.ingest;

import com.daydev.stormstar.Document;
import com.daydev.stormstar.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by robert on 28.05.2017.
 */
@RestController
public class IngestController {
    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    DocumentProducer documentProducer;

    @RequestMapping(value = "/{company}/{documentName}", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addDocument(String company, String documentName, @RequestBody String bodyContent) {
        ResponseEntity<String> result;
        try {
            Document document = documentRepository.findByCompanyAndName(company, documentName);

            documentProducer.send(document, bodyContent);
            result = new ResponseEntity<>("", HttpStatus.OK);
        } catch (Exception ex) {
            result = new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return result;
    }
}

