package com.daydev.stormstar.ingest;

/**
 * Created by robert on 28.05.2017.
 */

import com.daydev.stormstar.Document;
import com.daydev.stormstar.DocumentObj;
import com.daydev.stormstar.DocumentObjRepository;
import com.daydev.stormstar.DocumentRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class DocumentObjProducer {
    public final int LOOKUP_FIELD_RELEVANT_LEN = 50;
    Date date= new Date();
    @Autowired
    DocumentObjRepository documentObjRepository;


    public DocumentObjProducer() {
    }


    public void send(Document document, String json){
        JSONObject jsonObject = new JSONObject(json);

        DocumentObj documentObj = new DocumentObj();

        documentObj.setContent(json);
        documentObj.setDate(System.currentTimeMillis());
        documentObj.setField1(getValue(jsonObject, document.getField1()));
        documentObj.setField2(getValue(jsonObject, document.getField2()));
        documentObj.setField3(getValue(jsonObject, document.getField3()));
        documentObj.setField4(getValue(jsonObject, document.getField4()));
        documentObj.setField5(getValue(jsonObject, document.getField5()));

        documentObjRepository.save(documentObj);
    }

    private String getValue(JSONObject jsonObject, String fieldName){
        if (fieldName!=null){
            return jsonObject.getString(fieldName).substring(0, LOOKUP_FIELD_RELEVANT_LEN);
        }
        else return null;
    }

    public void close() {
    }
}
